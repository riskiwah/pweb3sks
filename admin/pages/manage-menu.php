<?php
	include("../func/CRUD.php");
	$query = mysqli_query($connect,"SELECT * FROM menus");

?>
<html>
  <head>
    <title>Dashboard Admin</title>
    <link rel="stylesheet" type="text/css" href="../css/allin.css" />
  </head>
    <body>
      <div class="header">
      <a href="#default" class="header">Welcome !</a>
      <div class="header-right">
        <a class="active" href="../admin">Home</a>
        <a class="#cari">Cari</a>
        <a class="#logout" href="../func/logout.php">Logout</a>
      </div>
    </div>
    <div class="row">
      <div class="column side">
        <a href="..">Dashboard Admin</a>
        <a href="manage-user.php">Manage User</a>
        <a href="manage-menu.php">Manage menu</a>
      </div>
      <div class="column middle">
        <h2>Hello Menu!</h2>
				<a href="tambah-menu.php">Tambahkan menu</a>
				<?php
					while ($doc = mysqli_fetch_array($query)) { ?>

				<div class="card">
					<div>
						nama : <?= $doc['name']?>
					</div>

						<img src="../../pic/<?= $doc['photo']?>">

					<div>
						price : <?= $doc['price']?>
					</div>
					<div>
						description : <?= $doc['description']?>
					</div>
					<div>
						<a href="edit-menu.php?id=<?php echo $doc['id'] ; ?>">Edit</a>
						<a href="hapus-menu.php?id=<?php echo $doc['id'] ; ?> " >Hapus</a>
					</div>
				</div>
			<?php }
				 ?>
      </div>
    </div>
    <div class="footer">
      <p>
        This is Footer
      </p>
    </div>
    </body>
</html>
