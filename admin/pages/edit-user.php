<?php
  include '../func/CRUD.php';
  $id = $_GET['id'];
  $edituser = SelectW('users', 'id', $id);
  $edituser = mysqli_fetch_array($edituser);
  // var_dump($edituser);
  // die;
?>
<html>
  <head>
    <title>Dashboard Admin</title>
    <link rel="stylesheet" type="text/css" href="../css/allin.css" />
  </head>
    <body>
      <div class="header">
      <a href="#default" class="header">Welcome !</a>
      <div class="header-right">
        <a class="active" href="../admin">Home</a>
        <a class="#cari">Cari</a>
        <a class="#logout" href="../func/logout.php">Logout</a>
      </div>
    </div>
    <div class="row">
      <div class="column side">
        <a href="#">Dashboard Admin</a>
        <a href="manage-user.php">Manage User</a>
        <a href="manage-menu.php">Manage Menu</a>
      </div>
      <div class="column middle">
        <form action="#" method="post" style="margin: 0 auto;">
          <input type="hidden" value="edit-register" name="allin" />
          <input type="hidden" value="<?php echo $id ?>" name="id" />
          <label>Username</label> <input type="text" name="username" value="<?= $edituser['username'] ?>" /><br />
          <label>Name</label> <input type="text" name="name" value="<?= $edituser['name'] ?>" /><br />
          <label>Email</label> <input type="email" name="email" value="<?= $edituser['email'] ?>"/><br />
          <label>Password</label> <input type="password" name="password" value="<?= $edituser['password'] ?>"/><br />
          <label>Address</label> <input type="text" name="address" value="<?= $edituser['address'] ?>"/><br />
          <label>Phone number</label> <input type="text" name="phone_number" value="<?= $edituser['phone_number'] ?>"/><br />
          <input type="submit" value="Edit!" name="submit" />
        </form>
      </div>
    </div>
    <div class="footer">
      <p>
        This is Footer
      </p>
    </div>
    </body>
</html>
