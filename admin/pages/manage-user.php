<?php
	include("../func/db.php");
	$query = mysqli_query($connect,"SELECT * FROM users");
?>
<html>
  <head>
    <title>Dashboard Admin</title>
    <link rel="stylesheet" type="text/css" href="../css/allin.css" />
  </head>
    <body>
      <div class="header">
      <a href="#default" class="header">Welcome !</a>
      <div class="header-right">
        <a class="active" href="../admin">Home</a>
        <a class="#cari">Cari</a>
        <a class="#logout" href="../func/logout.php">Logout</a>
      </div>
    </div>
    <div class="row">
      <div class="column side">
        <a href="..">Dashboard Admin</a>
        <a href="manage-user.php">Manage Users</a>
        <a href="manage-menu.php">Manage Menu</a>
      </div>
      <div class="column middle">
        <table style="border: 1px solid black;">
				<tr>
					<th width="1%">No</th>
					<th width="3%">username</th>
					<th width="12%">nama</th>
					<th width="15%">email</th>
					<th width="15%">address</th>
					<th width="3%">role</th>
					<th width="3%">created</th>
					<th width="5%">Aksi</th>
				</tr>
				<?php
					$i = 1;
					while ($doc = mysqli_fetch_array($query)) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $doc['username']; ?></td>
						<td><?php echo $doc['nama']; ?></td>
						<td><?php echo $doc['email']; ?></td>
						<td><?php echo $doc['address']; ?></td>
						<td><?php echo $doc['role']; ?></td>
						<td><?php echo $doc['created_at']; ?></td>
						<td>
							<a href="edit-user.php?id=<?php echo $doc['id'] ; ?>">Edit</a>
							<a href="hapus-data.php?id=<?php echo $doc['id'] ; ?> " >Hapus</a>
						</td>
					</tr>
				 <?php $i++;
				 } ?>
		</table>
      </div>
    </div>
    <div class="footer">
      <p>
        This is Footer
      </p>
    </div>
    </body>
</html>
