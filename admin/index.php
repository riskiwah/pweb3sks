<html>
  <head>
    <title>Dashboard Admin</title>
    <link rel="stylesheet" type="text/css" href="css/allin.css" />
  </head>
    <body>
      <div class="header">
      <a href="#default" class="header">Welcome !</a>
      <div class="header-right">
        <a class="active" href="../admin">Home</a>
        <a class="#cari">Cari</a>
        <a class="#logout" href="../func/logout.php">Logout</a>
      </div>
    </div>
    <div class="row">
      <div class="column side">
        <a href="#">Dashboard Admin</a>
        <a href="pages/manage-user.php">Manage User</a>
        <a href="pages/manage-menu.php">Manage Menu</a>
      </div>
      <div class="column middle">
        <h2>Welcome</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet pretium urna. Vivamus venenatis velit nec neque ultricies, eget elementum magna tristique. Quisque vehicula, risus eget aliquam placerat, purus leo tincidunt eros, eget luctus
          quam orci in velit. Praesent scelerisque tortor sed accumsan convallis.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet pretium urna. Vivamus venenatis velit nec neque ultricies, eget elementum magna tristique. Quisque vehicula, risus eget aliquam placerat, purus leo tincidunt eros, eget luctus
          quam orci in velit. Praesent scelerisque tortor sed accumsan convallis.</p>
      </div>
    </div>
    <div class="footer">
      <p>
        This is Footer
      </p>
    </div>
    </body>
</html>
