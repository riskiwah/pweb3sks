<?php
session_start();
if (isset($_SESSION)) {
  if (!isset($_SESSION['id'])) {
    $_SESSION['role'] = 'customers';
  }
}
 ?>

<html>
  <head>
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="css/design.css" />
  </head>
    <body>
      <div class="header">
      <a href="#default" class="header">Welcome !</a>
      <div class="header-right">
        <a class="active" href="home">Home</a>
        <a class="#register" href="register.php">Register</a>
        <a class="#login" href="login.php">Login</a>
        <a class="#about">About</a>
      </div>
    </div>
    <div class="row1">
      <div class="column" style="background-color:#F4F4F4;">
        <h2>type here</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere turpis sem, et commodo leo dictum at. Integer tincidunt quam non elit porttitor, et tempor est mollis. Mauris et viverra nisl. Pellentesque nec ligula interdum, varius justo et, placerat dui. Donec metus nisi, porta in congue nec, volutpat sed lectus. Mauris in pharetra dui. Suspendisse elementum quam vel lorem sodales viverra vitae vel ex. Pellentesque ornare vel neque non eleifend.
        </p>
      </div>
      <div class="column" style="background-color:#F4F4F4;">
        <h2>type here also</h2>
        <p>
          Lorem ipsum
        </p>
    </div>
    <div class="footer">
      <p>
        This is Footer
      </p>
    </div>
    </body>
</html>
